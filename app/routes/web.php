<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'WelcomeController@index');
Route::resource('tours','TourController');
// Route::resource('pages','PageController');
Route::resource('categories','CategoryController');
Route::resource('clients','ClientController');

Route::group(['middleware' => 'auth'], function () {

	Route::get('admin', function () {
		return redirect('/home');
	});

	Route::get('admin/tour/{id}', 'TourController@show');
	Route::post('admin/tour/{id}', 'TourController@update');
	Route::get('admin/tours/', 'TourController@index');
	Route::get('admin/tours/destroy/{id}', 'TourController@destroy');
	Route::post('admin/tour/putInHome/{id}', 'TourController@putInHome');
	Route::get('admin/tours/borradoCasiTotal', 'TourController@borradoCasiTotal');
	
	


	Route::get('admin/client/{id}', 'ClientController@show');
	Route::post('admin/client/{id}', 'ClientController@update');
	Route::get('admin/clients/', 'ClientController@index');
	Route::get('admin/clients/destroy/{id}', 'ClientController@destroy');
	Route::get('admin/clients/borradoCasiTotal', 'ClientController@borradoCasiTotal');


	Route::get('admin/category/{id}', 'CategoryController@show');
	Route::post('admin/category/{id}', 'CategoryController@update');
	Route::get('admin/categories/', 'CategoryController@index');

	Route::get('admin/welcome', 'WelcomeController@show');
	Route::post('admin/welcome/{id}', 'WelcomeController@update');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tour/{id}', 'TourController@showfront');
Route::get('/tours', 'TourController@indexfront');
Route::get('/tours/search/{tour_name}', ['as'=>'search', 'uses'=>'TourController@getTour']);


Route::post('contactus',[
'as'=>'contactus.store',
'uses'=>'ContactController@contactUsPost'
]);