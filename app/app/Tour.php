<?php

namespace App;

use App\Client;
use App\Category;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
	protected $fillable = [
        'name', 'slug', 'cover', 'description', 'content', 'client_id','categoriesids','publish'
    ];
    protected $dates = ['date_start','date_finish'];//se convierte en instancia de Carbon

    public function clients()
	{
	 return $this->belongsTo(Client::class);
	}

	public function categories()
	{
	 return $this->belongsToMany(Category::class);
	}

}
