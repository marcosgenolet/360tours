<?php

namespace App;
use App\Tour;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function tours()
	{
	 return $this->belongsToMany(Tour::class);
	}
}
