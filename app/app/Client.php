<?php

namespace App;
use App\Tour;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $fillable = [
        'name', 'email', 'phone'
    ];

    public function tours()
	{
	 return $this->hasMany(Tour::class);
	}
}
