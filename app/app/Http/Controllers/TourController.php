<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Traits\Macroable;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use App\Tour;
use App\Client;
use App\Category;
use Carbon\Carbon;
use Auth;


class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours = Tour::paginate(12);
        $tourshome = DB::table('tours')->where('in_home', 1)->get();
        $total = count(Tour::all());
        return view('backend.tours',compact(['tours','total','tourshome','thename']));
    }

    public function indexFront()
    {
        $tours = Tour::simplePaginate(20);
        $total = count(Tour::all());
        $thename = '';
        return view('frontend.galery',compact(['tours','total','thename']));
    }

    public function getTour(Request $request){
        $thename = request('tour_name');

        if($thename){
            $tours = Tour::query()->where('publish', true)->whereLike(['name'], $thename)->paginate(10);
        }else{
            $tours = Tour::paginate(20);
            $thename = '';
        }  
        $total = count($tours);
        return view('frontend.galery',compact(['tours','total','thename']));
    }

    public function putInHome(Request $request, $id){
        $tour = Tour::findOrFail($id);

        $valor = $request->get('inhome_'.$id);
        

        if(request('inhome_'.$id)){
            $tour->in_home = true;
        }else{
            $tour->in_home = false;
        }

        $tour->save();

        return Redirect::back()->with('message','Operation Successful !');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function publish(Request $request, $id)
    {
        $tour = Tour::findOrFail($id);
        $tour->publish = true;
        $tour->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newTour = new Tour;
        $newTour->name = $request->name;
        $newTour->slug =  $request->name;
        $newTour->save();
        
        return redirect('/admin/tour/'.$newTour->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tour = Tour::findOrFail($id);
        $clients = Client::all();
        $selectedCats = $tour->categories()->get();
        $categories = Category::all();
        return view('backend.tour',compact(['tour','clients','categories','selectedCats']));
    }

    public function showfront($id)
    {
        $tour = Tour::findOrFail($id);
        $clients = Client::all();
        $categories = Category::all();
        $selectedCats = $tour->categories()->get();
        $theClient = Client::findOrFail( $tour->client_id );
        return view('frontend.tour',compact(['tour','theClient','clients','selectedCats']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tour = Tour::findOrFail($id);

        if($tour->name != $request->name){
            $tour->name = $request->name;
        }
        
        if($tour->description != $request->description){
            $tour->description = $request->description;
        }

        //visible
        if(request('publicview')){
            $tour->publish = 1;
        }else{
            $tour->publish = 0;
        }


        $datestart = request('startdate');
        $dateend = request('enddate');
        //start date
        if($datestart != ''){
            $datestart = Carbon::parse(request('startdate'))->format('Y-m-d H:i');
            $start = Carbon::createFromFormat('Y-m-d H:i', $datestart)->toDateTimeString();
            $tour->date_start = $start;
        }else{
            $tour->date_start = NULL;
        }
        //end date
        if($dateend != ''){
            $dateend = Carbon::parse(request('enddate'))->format('Y-m-d H:i');
            $end = Carbon::createFromFormat('Y-m-d H:i', $dateend)->toDateTimeString();
            $tour->date_finish = $end;
        }else{
            $tour->date_finish = NULL;
        }

        //new client
        if($tour->client_id != $request->clientid){
            $tour->client_id = $request->clientid;
        }
        $tour->save();

        //categories 
        if($request->categoriesids != ''){
            $tour->categories()->detach();
            $resultado = json_decode($request->categoriesids);
            $tour->categories()->attach($resultado);
        }

        //content
        if($tour->content != $request->content){
            $tour->content = $request->content;
        }

        $tour->save();

        //cover
        $file_cover = request('coverimg', false);
        if ($file_cover and $file_cover->isValid()) {


            $filename_cover = "cover_tour_" . $tour->id . "." . $file_cover->getClientOriginalExtension();
            $file_cover->move('uploads/', $filename_cover);
            $tour->cover = $filename_cover;
            $tour->save();

            //cover thumbs
            //https://stackoverflow.com/questions/1855996/crop-image-in-php
            $image = imagecreatefromstring(file_get_contents('uploads/'.$tour->cover));
            $thumb_width = 350;
            $thumb_height = 220;

            $width = imagesx($image);
            $height = imagesy($image);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            if ( $original_aspect >= $thumb_aspect )
            {
               // If image is wider than thumbnail (in aspect ratio sense)
               $new_height = $thumb_height;
               $new_width = $width / ($height / $thumb_height);
            }
            else
            {
               // If the thumbnail is wider than the image
               $new_width = $thumb_width;
               $new_height = $height / ($width / $thumb_width);
            }

            $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

            // Resize and crop
            imagecopyresampled($thumb,
                               $image,
                               0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                               0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                               0, 0,
                               $new_width, $new_height,
                               $width, $height);


            imagejpeg($thumb, 'uploads/thumb_cover_tour_'.$tour->id.'.jpg', 80);
            
        }

        return redirect('/admin/tour/'.$id);
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = 0)
    {
        $tour = Tour::findOrFail( $id );

        

        if($tour != NULL) {
            DB::table('category_tour')->where('tour_id',$id)->delete();
            $tour->delete();
        }
        return redirect('/admin/tours');
    }

    public function borradoCasiTotal()
    {
        $tour = DB::table('tours')->where('id','<=',300);
        DB::table('category_tour')->where('tour_id','<=',300)->delete();
        $tour->delete();
        return redirect('/admin/tours');
    }
}
