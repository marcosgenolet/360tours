<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Contact;
use Mail;

class ContactController extends Controller
{
    public function contactUsPost(Request $request)
   	{

       $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required'
        ]);

       	Contact::create($request->all());
        Mail::send('email',
        array(

           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'user_message' => $request->get('message')

        ), function($message)


       {

       $message->from('techanical-atom@gmail.com');
       $message->to('user@example.com', 'Admin')
       ->subject('Contacto desde la web 360Tours');


      });


   return back()->with('success', 'Gracias! Nos pondremos en contacto a la brevedad');


   }
}
