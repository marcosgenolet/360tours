<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Tour;
use App\Welcome;
use App\Category;

class WelcomeController extends Controller
{
    //
    public function index()
    {
        //$tours = DB::table('tours')->where('in_home', 1)->take(10);
        $tours = Tour::where('in_home', 1)->take(10)->get();
    	$categorias = Category::all();


        return view('welcome',compact(['tours','categorias']));
    }
    public function show()
    {
        $welcome = Welcome::where('name','welcome')->get();
    	return view('backend.welcome',compact(['welcome']));
    }
    public function update(Request $request, $id)
    {
        $welcome = Welcome::findOrFail($id);

        //circulo
        if($welcome->circle_first != $request->circle_first){
            $welcome->circle_first = $request->circle_first;
        }
        if($welcome->circle_second != $request->circle_second){
            $welcome->circle_second = $request->circle_second;
        }
        if($welcome->circle_button != $request->circle_button){
            $welcome->circle_button = $request->circle_button;
        }
        //como funciona
        if($welcome->step1_title != $request->step1_title){
            $welcome->step1_title = $request->step1_title;
        }
        if($welcome->step1_content != $request->step1_content){
            $welcome->step1_content = $request->step1_content;
        }
        if($welcome->step2_title != $request->step2_title){
            $welcome->step2_title = $request->step2_title;
        }
        if($welcome->step2_content != $request->step2_content){
            $welcome->step2_content = $request->step2_content;
        }
        if($welcome->step3_title != $request->step3_title){
            $welcome->step3_title = $request->step3_title;
        }
        if($welcome->step3_content != $request->step3_content){
            $welcome->step3_content = $request->step3_content;
        }

        $welcome->save();
        return redirect('/admin/welcome/');
    }
}
