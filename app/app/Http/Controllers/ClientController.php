<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Tour;
use App\Client;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::simplePaginate(10);
        $total = count(Client::all());
        return view('backend.clients',compact(['clients','total']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newClient = new Client;
        $newClient->name = $request->name;
        $newClient->save();
        return redirect('/admin/client/'.$newClient->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);
        return view('backend.client',compact(['client']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        if($client->name != $request->name){
            $client->name = $request->name;
        }
        if($client->email != $request->email){
            $client->email = $request->email;
        }
        if($client->phone != $request->phone){
            $client->phone = $request->phone;
        }
        $client->save();
        return redirect('/admin/client/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail( $id );

        if($client != NULL) {
            //DB::table('category_tour')->where('tour_id',$id)->delete();
            $client->delete();
        }
        return redirect('/admin/clients');
    }
    // public function borradoCasiTotal()
    // {
    //     DB::table('clients')->whereBetween('id', [2, 51])->delete();
    //     return redirect('/admin/clients');
    // }
}
