<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Welcome extends Model
{
    protected $fillable = [
        'circle_first', 'circle_second', 'circle_button', 'step1_title', 'step1_content','step2_title','step2_content','step3_title','step3_content'
    ];
}