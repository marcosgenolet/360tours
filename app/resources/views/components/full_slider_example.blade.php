<div class="alert {{$type == 'danger'? 'alert-danger' : 'alert-info'}}">
    <div class="alert-title">{{ $title }}</div>
    <div class="contenido">{{ $slot }}</div>
</div>