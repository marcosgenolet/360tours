@extends('layouts.front')

@section('content')
<div class="filterarea container-fluid">
    <div class="container">
        <div class="row">
            @include('frontend.partials.filtergalery')
        </div>
    </div>
</div>
<div id="galery" class="container">
    <div class="row">
    @if (count($tours) > 0)
    	<div class="col-md-12">{{ $tours->links() }}</div>
    	@foreach( $tours as $tour)
    		<div class="col-md-3 col-sm-6">
    			<a href="{{ url('tour/'.$tour->id) }}">
	    			@include('frontend.partials.covertour')
    			</a>
    		</div>
    	@endforeach
    	<div class="col-lg-12">{{ $tours->links() }}</div>
    @else
    	No hay tours ingresados aún
    @endif
    </div>

</div>
@endsection