@extends('layouts.front')

@section('content')
<div class="container infotour">
    <div class="row">
    	<div class="col">
    		<h2>{{ $tour -> name }}</h2>
            @foreach ($selectedCats as $cat)
            <span class="tag">{{ $cat -> name }}</span>
            @endforeach
    	</div>
    </div>
    @if($tour -> description || $theClient )
    <div class="row">
        <div class="col">
            <div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne" aria-expanded="true"
                        aria-controls="collapseOne" class="showhide clearfix collapsed">
                            <p class="title">Información</p>
                            <i class="fa fa-plus-square-o fa-2x float-right" aria-hidden="true"></i>
                            <i class="fa fa-minus-square-o fa-2x float-right" aria-hidden="true"></i>
                        </a>
                      </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        <dl>
                            @if($theClient)
                            <dt>Cliente:</dt>
                            <dd>{{ $theClient -> name }}</dd>
                            @endif
                            @if($tour -> description)
                            <dt>Descripción:</dt>
                            <dd>{{ $tour -> description }}</dd>
                            @endif
                        </dl>
                      </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    @endif
</div>
<div class="container-fluid tourarea">
   	<div class="row">
    	<div class="col">
    		<?php $contenido = htmlspecialchars_decode ($tour -> content);
    		echo $contenido;
    		?>
    	</div>
   	</div>
</div>
@endsection