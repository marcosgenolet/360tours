<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand brand-diapo" href="#">
        <img src="{{asset('img/logo_360tours_diapo.png')}}" alt="Logo">
    </a>
    <a class="navbar-brand brand-color" href="#">
        <img src="{{asset('img/logo_360tours_color.png')}}" alt="Logo">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="toggler-icon"></span>
        <span class="toggler-icon"></span>
        <span class="toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse sub-menu-bar" id="mainNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="page-scroll js-scroll-trigger" href="#beneficios">BENEFICIOS</a>
            </li>
            <li class="nav-item">
                <a class="page-scroll js-scroll-trigger" href="#interactividad">INTERACTIVIDAD</a>
            </li>
            <li class="nav-item">
                <a class="page-scroll js-scroll-trigger" href="#galeria">GALERÍA</a>
            </li>
            <li class="nav-item">
                <a class="page-scroll js-scroll-trigger" href="#como-funciona">COMO FUNCIONA</a>
            </li>
            <li class="nav-item">
                <a class="page-scroll js-scroll-trigger" href="#contacto">CONTACTO</a>
            </li>
            <!-- @if (Route::has('login'))
            <li class="nav-item">
                @auth
                    <a class="page-scroll" href="{{ url('/home') }}">Home</a>
                @else
                    <a class="page-scroll" href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="page-scroll" href="{{ route('register') }}">Register</a>
                    </li>
                    @endif
                @endauth
            </li>
            @endif -->
        </ul>
    </div>

    <!-- <div class="navbar-btn d-none mt-15 d-lg-inline-block">
        <a class="menu-bar" href="#side-menu-right"><i class="lni-menu"></i></a>
    </div> -->
</nav> <!-- navbar -->