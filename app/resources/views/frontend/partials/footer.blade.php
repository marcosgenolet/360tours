<footer class="container-fluid">
    <div class="row justify-content-center">
        <section id="contacto" class="col-md-6">
            <div class="row align-items-center">
                <div class="col-md-12 text-center">
                    <h3>Contacto</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center links">
                    <a href="https://www.instagram.com/360tours.uy/" target="_blank" title="instagram"><i class="fa fa-instagram fa-3x"></i></a>
                    <a href="https://www.facebook.com/360-Tours-101130108275736" target="_blank" title="facebook"><i class="fa fa-facebook fa-3x"></i></a>
                    <a href="https://www.youtube.com/channel/UC-9M-qGwf5d8ErJuDwf5KLA/" target="_blank" title="youtube"><i class="fa fa-youtube-play fa-3x"></i></a>
                    <a href="https://api.whatsapp.com/send?phone=59895731997" target="_blank" title="whatsapp"><i class="fa fa-whatsapp fa-3x"></i></a>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#contactForm" title="mensaje"><i class="fa fa-envelope-o fa-3x"></i></a>
                </div>
            </div>
        </section>
    </div>
    <div class="row copyright">
        <div class="col-md-12 text-center">
            360Tours © 2020
        </div>
    </div>
</footer>
<div class="modal fade" id="contactForm" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Mensaje</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="clients">
                    {{ csrf_field() }}
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" placeholder="Nombre" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email" required>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" placeholder="Tel" required>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Mensaje</textarea>
                            </div>
                            @if(env('GOOGLE_RECAPTCHA_KEY'))
                             <div class="g-recaptcha"
                                  data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                             </div>
                            @endif
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                        
                    </div>
                </form>                
            </div>
        </div>    
    </div>
</div>