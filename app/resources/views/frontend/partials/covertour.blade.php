<div class="card">
    <div class="wrap-image">
		<img src="{{isset($tour -> cover) ? asset('uploads/thumb_'.$tour -> cover) : asset('img/cover_default.png')}}" class="card-img-top img-fluid" />
        <div class="portfolio-overlay d-flex align-items-center justify-content-center">
            <div class="portfolio-content">
                <div class="portfolio-icon">
                    <i class="fa fa-search-plus fa-2x" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
	<div class="card-body">
		<p class="card-title">{{$tour->name}}</p>
	</div>
</div>