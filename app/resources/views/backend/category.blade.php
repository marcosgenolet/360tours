@extends('layouts.edit')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card">
        <form id="thecategory" method="post" action="/admin/category/{{$category -> id}}" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="card-header">
            Categoría: <strong>{{ $category -> name }}</strong>
            <button type="submit" class="btn btn-primary btn-update float-right">Actualizar</button>
          </div>
          <div class="card-body">
            <div class="row top">
              <div class="col-md-6">   
                <div class="form-group">
                  <label for="name">Nombre</label>
                  <input type="text" name="name" id="name" class="form-control" placeholder="Nombre de la categoria" value="{{$category -> name}}" required>
                </div>
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea id="description" name="description" class="form-control" rows="3">{{isset($category -> description) ? $category -> description : '' }}</textarea>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection