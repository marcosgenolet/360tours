@extends('layouts.edit')

@section('content')

<div class="container">
@if (count($tours) > 0)
<div class="col-md-12">{{ $tours->links() }}</div>
<h2>Tours realizados: {{$total}}</h2>
<table class="table">
  	<thead>
    	<tr>
    		<th>ID</th>
        <th>Nombre</th>
        <th>Visible</th>
        <th>Ver</th>
    		<th>En home</th>
    		<th colspan="2">Acciones</th>
    	</tr>
   	</thead>
   	<tbody>
   		@foreach ($tours as $tour)
   		<tr>
    		<td>{{ $tour -> id }}</td>
    		<td>{{ $tour -> name }}</td>
    		<td>@if($tour -> publish == true) SI @else NO @endif</td>
        <td><a class="btn btn-default" href="{{url('tour/' . $tour -> id)}}">Ver</a></td>
        <td>
          <form id="putInHome_{{$tour -> id}}" method="post" action="/admin/tour/putInHome/{{$tour -> id}}" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="custom-control custom-switch form-group">
              <input type="checkbox" class="custom-control-input" name="inhome_{{$tour -> id}}" id="inhome_{{$tour -> id}}" rel="{{$tour -> id}}" @if($tour->in_home == true)? checked : '' @endif />
              <label class="custom-control-label" for="inhome_{{$tour -> id}}">home</label>
            </div>
          </form>
        </td>
    		<td><a class="btn btn-default" href="{{url('admin/tour/' . $tour -> id)}}">Editar</a></td>
    		<td><a onclick="return confirm('¿Realmente deseas eliminar este tour?');" href="{{url('admin/tours/destroy/' . $tour->id)}}">Borrar</a></td>

    	</tr>
   		@endforeach
   	</tbody>
    {{ $tours->links() }}
    
</table>
@else
<h2>No se cargaron tours hasta la fecha</h2>
@endif
</div>
<?php echo Session::get('message');?>
@endsection
