@extends('layouts.edit')

@section('content')

<form id="thewelcomepage" method="post" action="/admin/welcome/1" enctype="multipart/form-data">
	{{ csrf_field() }}
    <div class="row top">
      <div class="col-md-12">
          <h1>Welcome</h1>
          <button type="submit" class="btn btn-primary btn-update float-right">Actualizar</button>
      </div>
    </div>
    <!-- <fieldset>
    	<legend>Slider</legend>
    	<div class="form-group imagebuttons">
      	<img id="coverimg" src="{{isset($page -> slide1) ? asset('uploads/'.$page -> slide1) : asset('img/cover_default.png')}}"/>
          <div class="boton btn btn-primary" type="button">Nueva imagen
            	<input type="file" id="slide1" name="slide1" class="form-control" onchange="readURL(this,'slide1');" />
          </div>
    	</div>
    </fieldset> -->
    <fieldset>
      <legend>Cabecera Circulo</legend>
      <div class="form-group">
        <label for="name">Párrafo 1</label>
        <input type="text" name="circle_first" id="circle_first" class="form-control" placeholder="Párrafo 1" value="{{isset($welcome -> circle_first)? $welcome -> circle_first : ''}}">

      </div>
      <div class="form-group">
        <label for="name">Párrafo 2</label>
        <input type="text" name="circle_second" id="circle_second" class="form-control" placeholder="Párrafo 2" value="{{isset($welcome -> circle_second)? $welcome -> circle_second : ''}}">
      </div>
      <div class="form-group">
        <label for="name">Texto Botón</label>
        <input type="text" name="circle_button" id="circle_button" class="form-control" placeholder="Párrafo 2" value="{{isset($welcome -> circle_button)? $welcome -> circle_button : ''}}">
      </div>
    </fieldset>
    <fieldset>
      <legend>Cómo funciona</legend>
      <div class="form-group">
        <label for="name">Paso 1 Título</label>
        <input type="text" name="step1_title" id="step1_title" class="form-control" placeholder="Título" value="{{isset($welcome -> step1_title)? $welcome -> step1_title : ''}}">
      </div>
      <div class="form-group">
        <label for="name">Paso 1 Contenido</label>
        <input type="text" name="step1_content" id="step1_content" class="form-control" placeholder="Contenido" value="{{isset($welcome -> step1_content)}}">
      </div>
      <div class="form-group">
        <label for="name">Paso 2 Título</label>
        <input type="text" name="step2_title" id="step2_title" class="form-control" placeholder="Título" value="{{isset($welcome -> step2_title)? $welcome -> step2_title : ''}}">
      </div>
      <div class="form-group">
        <label for="name">Paso 2 Contenido</label>
        <input type="text" name="step2_content" id="step2_content" class="form-control" placeholder="Contenido" value="{{isset($welcome -> step2_content)? $welcome -> step2_content : ''}}">
      </div>
      <div class="form-group">
        <label for="name">Paso 3 Título</label>
        <input type="text" name="step3_title" id="step3_title" class="form-control" placeholder="Título" value="{{isset($welcome -> step3_title)? $welcome -> step3_title : ''}}">
      </div>
      <div class="form-group">
        <label for="name">Paso 3 Contenido</label>
        <input type="text" name="step3_content" id="step3_content" class="form-control" placeholder="Contenido" value="{{isset($welcome -> step3_content)? $welcome -> step3_content : ''}}">
      </div>
    </fieldset>
</form>


@endsection