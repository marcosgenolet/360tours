<legend>Cover</legend>
<div class="form-group imagebuttons">
	<img class="frame" src="{{isset($item -> cover) ? asset('uploads/'.$item -> cover) : asset('img/cover_default.png')}}"/>
	<div class="boton btn btn-primary" type="button">Nueva imagen
	  	<input type="file" id="coverimg" name="coverimg" class="form-control" onchange="readURL(this,'frame');" />
	</div>
</div>