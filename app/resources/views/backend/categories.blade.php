@extends('layouts.edit')

@section('content')

<div class="container">
@if (count($categories) > 0)
<h2>Categorías creadas: {{count($categories)}}</h2>
<table class="table">
  	<thead>
    	<tr>
    		<th>ID</th>
    		<th>Nombre</th>
    		<th colspan="2">Acciones</th>
    	</tr>
   	</thead>
   	<tbody>
   		@foreach ($categories as $category)
   		<tr>
    		<td>{{ $category -> id }}</td>
    		<td>{{ $category -> name }}</td>
    		<td><a class="btn btn-default" href="{{url('admin/category/' . $category -> id)}}">Edit</a></td>
    		<td>Borrar</td>
    	</tr>
   		@endforeach
   	</tbody>
    
</table>
@else
<h2>No se cargaron categorías hasta la fecha</h2>
@endif
</div>
@endsection