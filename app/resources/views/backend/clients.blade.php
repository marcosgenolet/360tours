@extends('layouts.edit')

@section('content')

<div class="container">
@if (count($clients) > 0)
<div class="col-md-12">{{ $clients->links() }}</div>
<h2>Clientes ingresados: {{$total}}</h2>

<table class="table">
  	<thead>
    	<tr>
    		<th>ID</th>
        <th>Nombre</th>
        <th>Email</th>
    		<th>Tel</th>
    		<th colspan="2">Acciones</th>
    	</tr>
   	</thead>
   	<tbody>
   		@foreach ($clients as $client)
   		<tr>
    		<td valign="middle">{{ $client -> id }}</td>
        <td valign="middle">{{ $client -> name }}</td>
        <td valign="middle">{{ $client -> email }}</td>
    		<td valign="middle">{{ $client -> phone }}</td>
    		<td valign="middle"><a class="btn btn-default" href="{{url('admin/client/' . $client -> id)}}">Edit</a></td>
    		<td valign="middle"><a onclick="return confirm('¿Realmente deseas eliminar este cliente?');" href="{{url('admin/clients/destroy/' . $client->id)}}">Borrar</a></td>
    	</tr>
   		@endforeach
   	</tbody>
    
</table>
@else
<h2>No se cargaron clientes hasta la fecha</h2>
@endif
</div>
<!-- <a onclick="return confirm('¿Realmente deseas eliminar casi todos los tour?');" href="{{url('admin/clients/borradoCasiTotal')}}">Borrar casi todo</a> -->
@endsection