@extends('layouts.edit')

@section('content')

<div class="container">
  <div class="row">
    <div class="col">
      <div class="card">
        <form id="thetour" method="post" action="/admin/tour/{{$tour -> id}}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="card-header">
            Tour <strong>{{ $tour -> name }}</strong>
            <button type="submit" class="btn btn-primary btn-update float-right">Actualizar</button>
          </div>
          <div class="card-body">
            <div class="row top">
              <div class="col-md-6">  
                <!-- GENERAL -->
                <div class="form-group">
                  <label for="name">Nombre</label>
                  <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del tour" value="{{$tour -> name}}" required>
                </div>
                
                <div class="form-group imagebuttons">
                  <div class="row">
                    <div class="col-6">
                      <img id="covimg" src="{{isset($tour -> cover) ? asset('uploads/thumb_'.$tour -> cover) : asset('img/cover_default.png')}}" class="img-fluid" />
                    </div>
                    <div class="col-6">
                      <div class="custom-file">
                        <input type="file" id="coverimg" name="coverimg" class="custom-file-input" onchange="readURL(this);">
                        <label class="custom-file-label" for="coverimg">Nueva imagen</label>
                      </div>
                    </div>
                  </div>
              	</div>
                
                <div class="form-group">               
                  <label>Categorias</label>
                  @if (count($categories) > 0)
                    
                    @foreach ($categories as $category)
                      <div class="form-check cat-check">
                        <input class="form-check-input" type="checkbox" value="{{$category -> id}}" id="category_{{$category -> id}}" >
                        <label class="form-check-label" for="category_{{$category -> id}}">
                          {{$category -> name}}
                        </label>
                      </div>
                    @endforeach
                    <input type="hidden" value="" name="categoriesids" id="categoriesids" >
                  @else
                    No hay categorías ingresadas.
                  @endif
                </div>


                @foreach ($selectedCats as $selected)

                <script type="text/javascript">
                  $('#category_'+{{$selected -> id}}).prop( "checked", true );
                </script>

                @endforeach

                
                <!-- <div class="form-group">
                    <label for="enddate">Fecha de finalización</label>
                    <input id="enddate" name="enddate" type="text" class="form-control" value="{{isset($tour->date_finish) ? $tour->date_finish->format('d-m-Y') : ''}}">
                </div> -->
                <!-- DESCRIPCION -->
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea id="description" name="description" class="form-control" rows="3">{{isset($tour -> description) ? $tour -> description : '' }}</textarea>
                </div>
                
              </div>

              <div class="col-md-6">
                
                <!-- Default switch -->
                <div class="custom-control custom-switch form-group">
                  <input type="checkbox" class="custom-control-input" name="publicview" id="publicview" @if($tour->publish)? checked : '' @endif>
                  <label class="custom-control-label" for="publicview">Visible al público</label>
                </div>

                @if (count($clients) > 0)
                <div class="form-group">
                  <label for="clientid">Cliente</label>
                  <select class="form-control" id="clientid" name="clientid">
                    <option value="1">Sin asignar</option>
                    @foreach ($clients as $client)
                      <option value="{{$client -> id}}" @if($tour -> client_id == $client -> id) selected @endif >{{ $client -> name }}</option>
                    @endforeach
                  </select>
                </div>
                @else
                No hay clientes ingresados.
                @endif
                <!-- FECHA -->
                <div class="form-group">
                    <label for="startdate">Fecha</label>
                    <input id="startdate" name="startdate" type="date" class="form-control" value="{{isset($tour->date_start) ? $tour->date_start->format('Y-m-d') : ''}}">
                    <!-- <input id="hstartdate" name="hstartdate" type="hidden" value="{{isset($tour->date_start) ? $tour->date_start->format('Y-m-d') : null}}"> -->
                </div>
                <!-- CODIGO -->
                <div class="form-group">
                    <label for="content">Código</label>
                    <textarea id="content" name="content" class="form-control" rows="3">{{isset($tour -> content) ? $tour -> content : '' }}</textarea>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script>




    var readURL = function(input) {

        

        if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
            console.log('anda');
            
              $('#covimg').attr('src', e.target.result);
            
          };
          reader.readAsDataURL(input.files[0]);
        }
      }
    $(document).ready(function(){

      
      var selectCheck = function(who){
        
      }

      var selectedCategories = [];

      $('.cat-check input[type="checkbox"]').click(function(){
        selectedCategories = [];
        $('.cat-check input[type="checkbox"]').each(function () { 
          if($(this).prop("checked") == true){
              selectedCategories.push($(this).val());
          }
        });

        $('#categoriesids').val(JSON.stringify(selectedCategories));
        console.log($('#categoriesids').val());


      });
    });
  </script>

  @endsection