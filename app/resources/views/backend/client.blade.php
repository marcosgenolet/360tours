@extends('layouts.edit')

@section('content')
<div class="container">
	<div class="row">
		<div class="col">
			<div class="card">
				<form id="thetour" method="post" action="/admin/client/{{$client -> id}}" enctype="multipart/form-data">
				{{ csrf_field() }}
					<div class="card-header">
						Cliente: <strong>{{ $client -> name }}</strong>
						<button type="submit" class="btn btn-primary btn-update float-right">Actualizar</button>
					</div>
					<div class="card-body">
						<div class="row top">
							<div class="col-md-6">  
								<!-- GENERAL -->
								<div class="form-group">
								  <label for="name">Nombre</label>
								  <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del tour" value="{{$client -> name}}" required>
								</div>
								<div class="form-group">
								  <label for="email">Email</label>
								  <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{$client -> email}}" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="phone">Tel</label>
								  <input type="tel" name="phone" id="phone" class="form-control" placeholder="Tel" value="{{$client -> phone}}" >
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection