<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!--====== Required meta tags ======-->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="Creación de Tours virtuales en 360° con info interactiva, listos para publicar y compartir">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="360,tour 360,visita 360,vista 360,camara 360,dron,uruguay">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!--====== Line Icons css ======-->
        <link rel="stylesheet" href="{{ asset('css/LineIcons.css')}}">

        <!--====== Slick css ======-->
        <link rel="stylesheet" href="{{ asset('css/slick.css')}}" >

        <!--====== Style css ======-->
        <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css')}}" >
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" >
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" >
        

    </head>
    <body class="front">
    <!--====== PRELOADER PART START ======-->
    <div class="preloader">
        <div class="loader">
            <div class="ytp-spinner">
                <div class="ytp-spinner-container">
                    <div class="ytp-spinner-rotator">
                        <div class="ytp-spinner-left">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                        <div class="ytp-spinner-right">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="header-area">
        <div class="navbar-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @include('frontend.partials.navbar')
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- navbar area -->
        
        <div id="home" class="slider-area">
            <!-- @full_slider_example(['type' => 'je'])
                @slot('title')
                    mi titulo!
                @endslot
                Contenido que el inyecto al componente
            @endfull_slider_example -->
            
            <div id="carouselOne" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselOne" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselOne" data-slide-to="1"></li>
                    <li data-target="#carouselOne" data-slide-to="2"></li>
                </ol>

                
                
                <!-- @include('frontend.components.sliderFull') -->
                <div class="carousel-inner">
                    <div class="carousel-item bg_cover active" style="background-image: url({{ asset('images/slide1.jpg')}})">
                    </div> <!-- carousel-item -->

                    <div class="carousel-item bg_cover" style="background-image: url({{ asset('images/slide2.jpg')}})">
                    </div> <!-- carousel-item -->

                    <div class="carousel-item bg_cover" style="background-image: url({{ asset('images/slide3.jpg')}})">
                    </div> <!-- carousel-item -->
                </div> <!-- carousel-inner -->

                <div class="hero-text">
                    <h2>Creamos Tours virtuales de 360° con info interactiva, listos para publicar y compartir!</h2>
                    <p>Somos la solución para dar vida a tu vivienda en venta o alquiler, comercio o evento.<br>Personalizamos el tour de acuerdo a tus necesidades.</p>
                    <a class="btn btn-primary" href="#">Contactarme</a>
                </div>
            </div> <!-- carousel -->
            
        </div>

    </section>
    
    <section id="beneficios" class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Beneficios</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="item col-md-3 col-sm-6">
                <img src="{{asset('images/ico_mas.png')}}" />
                <p>Añade valor al recorrido con información de interés. Resalta las virtudes de tu propuesta.</p>
            </div>
            <div class="item col-md-3 col-sm-6">
                <img src="{{asset('images/ico_cafe.png')}}" />
                <p>El visitante no necesita moverse de su casa u oficina.</p>
            </div>
            <div class="item col-md-3 col-sm-6">
                <img src="{{asset('images/ico_persona.png')}}" />
                <p>Es una experiencia inmersiva.</p>
            </div>
            <div class="item col-md-3 col-sm-6">
                <img src="{{asset('images/ico_estrellas.png')}}" />
                <p>Destaca tu propuesta frente al resto.</p>
            </div>
        </div>
    </section>

    <section id="interactividad" class="container-fluid">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="align-items-center col-sm-12 col-md-7 col-lg-5">
                    <h3>Cómo se muestra y trabaja la información interactiva</h3>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <div class="video-cover">
                        <a href="#">
                            <img class="play" src="{{asset('images/play.png')}}" />
                            <img class="cover img-fluid" src="{{asset('images/picture.jpg')}}" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
     
    <section id="galeria" class="portfolio-area container-fluid">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-20">
                        <h3>Galeria</h3>
                        <p class="text"></p>
                    </div> <!-- row -->
                </div>
            </div> <!-- row -->
           

            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-menu pt-30 text-center">
                        <ul>
                            <li data-filter="*" class="active">TODAS LAS CATEGORÍAS</li>
                            @foreach($categorias as $category)
                            <li data-filter=".cat_{{$category->id}}">{{$category->name}}</li>
                            @endforeach
                        </ul>
                    </div> <!-- portfolio menu -->
                </div>
            </div> <!-- row -->
            <div class="row grid">
                @foreach($tours as $tour)
                
                <?php $selectedCats = $tour->categories()->get(); ?>
                <div class="col-lg-4 col-sm-6 @foreach ($selectedCats as $cat) {{ 'cat_'.$cat -> id}} @endforeach">
                    <div class="single-portfolio mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <a href="{{url('tour/'.$tour->id)}}" title="{{$tour->name}}">
                        <div class="portfolio-image">
                            <img src="{{asset('uploads/thumb_'.$tour->cover)}}" alt="" class="img-fluid">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <i class="lni-zoom-in"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title">{{ $tour->name }}</h4>
                        </div>
                        </a>
                    </div> <!-- single portfolio -->
                </div>
                
                @endforeach
            </div> <!-- row -->
            <div class="row">
                <div class="col text-center">
                    <a href="{{url('tours')}}" class="view_all">Ver todos los tours <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div> <!-- container -->
    </section>
    <div class="bottom container-fluid">
        <section id="como-funciona" >
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>¿Cómo funciona?</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div id="idea" class="step col-md-3 align-items-center">
                    <figure>
                        <img src="{{asset('images/ico_luz.png')}}" class="img-fluid"/>
                    </figure>
                    <p class="title">Te escuchamos</p>
                    <p>Definimos la idea del Tour <br>y los puntos a destacar</p>
                </div>
                <div id="accion" class="step col-md-3 align-items-center">
                    <figure>
                        <img src="{{asset('images/ico_lente.png')}}" class="img-fluid"/>
                    </figure>
                    <p class="title">Luz, cámara, acción!</p>
                    <p>Te visitamos con nuestras cámaras 360 y drones.</p>
                </div>
                <div id="publicar" class="step col-md-3 align-items-center">
                    <figure>
                        <img src="{{asset('images/ico_redes.png')}}" class="img-fluid"/>
                    </figure>
                    <p class="title">A compartir!</p>
                    <p>Tour disponible en nuestra web y Youtube, listo para compartir</p>
                </div>
            </div>
        </section>
        <div class="row justify-content-center">
            <section id="contacto" class="col-md-6">
                <div class="row align-items-center">
                    <div class="col-md-12 text-center">
                        <h3>Contacto</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center links">
                        <a href="https://www.instagram.com/360tours.uy/" target="_blank" title="instagram"><i class="fa fa-instagram fa-3x"></i></a>
                        <a href="https://www.facebook.com/360-Tours-101130108275736" target="_blank" title="facebook"><i class="fa fa-facebook fa-3x"></i></a>
                        <a href="https://www.youtube.com/channel/UC-9M-qGwf5d8ErJuDwf5KLA/" target="_blank" title="youtube"><i class="fa fa-youtube-play fa-3x"></i></a>
                        <a href="https://api.whatsapp.com/send?phone=59895731997" target="_blank" title="whatsapp"><i class="fa fa-whatsapp fa-3x"></i></a>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#contactForm" title="mensaje"><i class="fa fa-envelope-o fa-3x"></i></a>
                    </div>
                </div>
            </section>
        </div>
        <div class="row copyright">
            <div class="col-md-12 text-center">
                360Tours © 2020
            </div>
        </div>
    </div>
        
        <a href="#" class="back-to-top"><i class="lni-chevron-up"></i></a>

        

        <div class="modal fade" id="contactForm" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Mensaje</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="clients">
                            {{ csrf_field() }}
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="name" placeholder="Nombre" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="name" placeholder="Tel" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Mensaje</textarea>
                                    </div>
                                    @if(env('GOOGLE_RECAPTCHA_KEY'))
                                     <div class="g-recaptcha"
                                          data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
                                     </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                                
                            </div>
                        </form>                
                    </div>
                </div>    
            </div>
        </div>
        <script src="{{ asset('js/wow.min.js') }}"></script>

        <script src="{{ asset('js/app.js') }}"></script>

        <script src="{{ asset('jquery-easing/jquery.easing.min.js') }}"></script>

        <script src="{{ asset('js/main.js') }}"></script>

        <!-- <script src="{{ asset('js/popper.min.js')}}"></script> -->

        <!--====== Slick js ======-->
        <script src="{{ asset('js/slick.min.js')}}"></script>

        <!--====== Isotope js ======-->
        <script src="{{ asset('js/isotope.pkgd.min.js')}}"></script>

        <!--====== Images Loaded js ======-->
        <script src="{{ asset('js/imagesloaded.pkgd.min.js')}}"></script>

        <!-- captcha -->
        <script src='https://www.google.com/recaptcha/api.js'></script>


        <script type="text/javascript">
            //===== Isotope Project 3
            $('.container').imagesLoaded(function () {
                var $grid = $('.grid').isotope({
                    // options
                    transitionDuration: '1s'
                });

                // filter items on button click
                $('.portfolio-menu ul').on('click', 'li', function () {
                    var filterValue = $(this).attr('data-filter');
                    $grid.isotope({
                        filter: filterValue
                    });
                });

                //for menu active class
                $('.portfolio-menu ul li').on('click', function (event) {
                    $(this).siblings('.active').removeClass('active');
                    $(this).addClass('active');
                    event.preventDefault();
                });
            });
        </script>
    </body>
</html>
