<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Creación de Tours virtuales en 360° con info interactiva, listos para publicar y compartir">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="360,tour 360,visita 360,vista 360,camara 360,dron,uruguay">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css')}}" >
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body id="front" class="front interna">
    @include('frontend.partials.navbarinterna')
    <main>
        @yield('content')
    </main>
    @include('frontend.partials.footer')
</body>
</html>
