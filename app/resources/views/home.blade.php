@extends('layouts.edit')

@section('content')
<div class="container home">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <p class="title">Tours</p><p>Total {{$totalTours}}</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newtour">
                    Nuevo Tour
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body clients">
                    <p class="title">Clientes</p><p>Total: {{$totalClients}}</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newclient">
                    Nuevo Cliente
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <p class="title">Categorías</p><p>Total: {{$totalCategories}}</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newcategory">
                    Nueva Categoría
                    </button>
                </div>
            </div>
        </div>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
</div>
<!-- MODALES -->

<div class="modal fade" id="newtour" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Nuevo Tour</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="tours">
                    {{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="name">Nombre del Tour</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del tour" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar y continuar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>                
            </div>
        </div>    
    </div>
</div>
<div class="modal fade" id="newclient" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Nuevo Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="clients">
                    {{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="name">Nombre del Cliente</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del cliente" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar y continuar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>                
            </div>
        </div>    
    </div>
</div>

<div class="modal fade" id="newcategory" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Nueva Categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="categories">
                    {{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="name">Nombre de la Categoría</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre de la Categoría" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar y continuar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>                
            </div>
        </div>    
    </div>
</div>
@endsection
