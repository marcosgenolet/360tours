<?php
use App\User;
use App\Tour;
use App\Client;
use App\Welcome;
use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        // $this->call(UsersTableSeeder::class);

        User::truncate();
        Tour::truncate();
        Client::truncate();
        Category::truncate();
        Welcome::truncate();

        $cantidadUsuarios = 1;
        $cantidadWelcomes = 1;
        $cantidadTours = 300;
        $cantidadClientes = 50;
        $cantidadCategories = 3;
       

        factory(User::class , $cantidadUsuarios)->create();

        factory(Client::class , $cantidadClientes)->create();

        factory(Category::class , $cantidadCategories)->create();

        factory(Welcome::class , $cantidadWelcomes)->create();


        factory(Tour::class , $cantidadTours)->create()->each(
            function($tours){
                $categorias = Category::all()->random(mt_rand(1,3))->pluck('id');
                $tours->categories()->attach($categorias);
            }
        );
    }
}
