<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->timestamps();
            $table->string('name');
            $table->string('slug');
            $table->boolean('publish')->default(false);
            $table->string('cover')->nullable();
            $table->string('description',1000)->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_finish')->nullable();
            $table->string('content',1000)->nullable();
            $table->boolean('in_home')->default(false);
            $table->integer('client_id')->unsigned()->nullable();

            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
