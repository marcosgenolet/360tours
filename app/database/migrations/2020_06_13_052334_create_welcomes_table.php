<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWelcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('welcomes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name')->default('welcome');
            $table->string('circle_first')->nullable();
            $table->string('circle_second')->nullable();
            $table->string('circle_button')->nullable();
            $table->string('step1_title')->nullable();
            $table->string('step1_content')->nullable();
            $table->string('step2_title')->nullable();
            $table->string('step2_content')->nullable();
            $table->string('step3_title')->nullable();
            $table->string('step3_content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('welcomes');
    }
}
