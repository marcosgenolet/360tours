<?php

use Faker\Generator as Faker;
use App\User;
use App\Welcome;
use App\Tour;
use App\Client;
use App\Category;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        // 'name' => $faker->name,
        'name' => 'marcos',
        //'email' => $faker->unique()->safeEmail,
        'email' => 'marcosgenolet@gmail.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'password' => 'socram53',
        'remember_token' => Str::random(10),
    ];
});

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['propiedad', 'negocio', 'evento']),
        'slug' => $faker->word,
        'description' => $faker->paragraph(1),
    ];
});

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
    ];
});


$factory->define(App\Tour::class, function (Faker $faker) {    
    return [
        'name' => $faker->word,
        'slug' => $faker->word,
        'cover' => $faker->randomElement(['cover_tour_1.jpg', 'cover_tour_2.jpg']),
        'publish' => $faker->randomElement([0, 1]),
        //'cover' => 'cover_tour_1.jpg',
        'description' => $faker->paragraph(1),
        'content' => '<iframe src="https://www.theasys.io/viewer/8FKl5pt0fNMjHq08IxL730DhOJJ59J" allowfullscreen="true" frameborder="0" scrolling="no" allow="vr;gyroscope;accelerometer" width="1280" height="720" style="border:none;"></iframe>',
        'date_start' => $faker->date,
        'client_id' => Client::all()->random()->id,
    ];
});

$factory->define(App\Welcome::class, function (Faker $faker) {
    return [
        'name' => 'welcome'
    ];
});
